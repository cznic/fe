# fe
Command fe (Fix Examples) corrects errors produced by examples while running go test.

Installation

    $ go get modernc.org/fe

Documentation: [godoc.org/modernc.org/fe](http://godoc.org/modernc.org/fe)
